.PHONY: help all clean
.DEFAULT_GOAL = all

EXE=app
SRC=$(wildcard src/*.c)
OBJ=$(addprefix build/,$(SRC:src/%.c=%.o))
CC=gcc

FLAGS=-g -Wall -Wextra -pedantic -ansi
LIBS=-lm

help: ## Display the availables commands
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-10s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

all: $(OBJ) ## Run the compilation of the app
	$(CC) -o $(EXE) $^ $(LIBS)

build/%.o: src/%.c
	@mkdir -p build
	$(CC) $(FLAGS) -o $@ -c $<

clean: ## Clean all .object files
	rm -f build/*.o $(EXE)