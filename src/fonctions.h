/* -------------------------------------------------------------------- */
/* 						Fichier d'entête autresFonctions				*/
/*                                                             			*/
/* Contient les prototypes des fonctions autres.						*/
/*																		*/
/* -------------------------------------------------------------------- */

#ifndef FONCTIONS
#define FONCTIONS

#include "arbre.h"

char ** Chargement(int *Error, int *nbMots, char *nomFichier);
void LibererMots(char ** mots, int nbMots);

#endif