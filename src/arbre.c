/* -------------------------------------------------------------------- */
/*            Fichier contenant les fonctions de l'arbre 	            	*/
/*                                                                      */
/* -------------------------------------------------------------------- */
#include "arbre.h"

/* ------------------------------------------------------------------------ */
/* InitNoeud   		       Initialise un noeud 		 	                    			*/
/*                                                                      		*/
/* En entrée: 											                  											*/
/*                                                                      		*/
/* En sortie: Nouveau noeud																									*/
/*                                                                      		*/
/* Locales : n: noeud à initialiser																					*/
/* ------------------------------------------------------------------------ */
Noeud_t *InitNoeud(){
		Noeud_t *n;

		n=(Noeud_t *)malloc(sizeof(Noeud_t));
		if(n!=NULL)
		{
			n->vertical=NULL;
			n->horizontal=NULL;
		}
		else
		{
			fprintf(stderr, "Problème d'allocation !\n");
		}
		return n;
}

/* ------------------------------------------------------------------------ */
/* Insertion          Insére un noeud					 	                						*/
/*                                                                      		*/
/* En entrée: a pointeur sur le noeud précédent 					        					*/
/*			  c caractére à insérer dans le nouveau noeud 											*/
/*                                                                      		*/
/* En sortie: adresse du nouveau noeud inséré																*/
/*                                                                      		*/
/* Locales : n: nouveau noeud																								*/
/* ------------------------------------------------------------------------ */
Noeud_t * Insertion(Noeud_t **prec, char c){
	Noeud_t *n = NULL;

	if(c != '\0'){
		n = InitNoeud();
		n->c = tolower(c);
		if(prec != NULL){
			if(*prec != NULL)
				n->horizontal = *prec;
			*prec = n;
		}
	}
	return n;
}


/* ------------------------------------------------------------------------ */
/* Recherche          Recherche la position d'insertion d'un mot 		 				*/
/*                                                                      		*/
/* En entrée: a pointeur sur l'arbre 					                 							*/
/*			  m mot à insérer dans l'arbre				 															*/
/*                                                                      		*/
/* Locales : prec: pointeur précédent, fin: condition d'arrêt								*/
/* ------------------------------------------------------------------------ */
Noeud_t** Recherche(Noeud_t **a , char ** m){
	Noeud_t **prec = a;
	int fin = 1;

	if(*a != NULL)
	{
		while(fin && **m != '\0' && *prec != NULL){
			if(tolower(**m) == tolower((*prec)->c)){
				if(*(*m+1) == '\0')
					fin = 0;
				else
					prec = &((*prec)->vertical);
				++(*m);
			}
			else{
				if(tolower((*prec)->c) < tolower(**m)){
					prec = &((*prec)->horizontal);
				}
				else{
					fin = 0;
				}
			}
		}
	}

	return prec;
}

/* ------------------------------------------------------------------------ */
/* InsertionMot          Insére un mot dans l'arbre		 	                		*/
/*                                                                      		*/
/* En entrée: a pointeur sur l'arbre 					                 							*/
/*			  m mot à insérer dans l'arbre				 															*/
/*                                                                      		*/
/* Locales : prec: pointeur précédent, cour: pointeur courant								*/
/* ------------------------------------------------------------------------ */
void InsertionMot(Noeud_t ** a , char * m){
	Noeud_t **prec=NULL, *cour;

	if(m && *m != '\0'){
		prec = Recherche(a, &m);
		cour = Insertion(prec, *m);
		if(*a == NULL)
			*a = cour;
		while(*(m+1) != '\0'){
			++m;
			prec = &(cour->vertical);
			cour = Insertion(prec, *m);
		}

		(*prec)->c = toupper((*prec)->c);
	}
}

/* -------------------------------------------------------------------------- */
/* AffichageArbre    	Affiche l'arbre	dans l'ordre alphabétique	 							*/
/*                                                                      			*/
/* En entrée: a pointeur sur l'arbre 					                 								*/
/*			  m mot à insérer dans l'arbre				 																*/
/*                                                                      			*/
/* Locales : p: pile pour la dérécursif., cour: pointeur courant dans l'arbre */
/* 		tab: pile p sous forme de tableau, fin: condition d'arrêt, i: index 		*/
/* 		j: index, code: code de retour																					*/
/* -------------------------------------------------------------------------- */
void AffichageArbre(Noeud_t *a, char *motif) {
	pile_t *p;
	Noeud_t *cour;
	Noeud_t **tab;
	int fin=0,i,j=0,code;

	printf("\n------AFFICHAGE DE L'ARBRE------\n");
	p=initPile(30);
	cour = a;
	while(!fin){
		while(cour != NULL){
			empiler(p, cour);
			if(isupper(cour->c)){
				tab = pileToTab(p);
				while (motif && motif[j+1] != '\0'){
					printf("%c", motif[j]);
					++j;
				}
				j=0;
				for(i=0; i < p->nb_elements; ++i){
					printf("%c", (tab[i])->c);
					tab[i] = NULL;
				}
				free(tab);
				printf("\n");
			}

			cour = cour->vertical;
		}
		if(!estPileVide(p)){
			cour = depiler(p,&code);
			cour = cour->horizontal;
		}
		else{
			fin = 1;
		}
	}
	printf("\n");
	libererPile(p);
}

/* ------------------------------------------------------------------------ */
/* RechercheMotif     		Recherche un motif dans l'arbre			   						*/
/*                                                                      		*/
/* En entrée: a pointeur sur le premier noeud de l'arbre 			 							*/
/*			  motif: chaine motif à rechercher dans l'arbre 										*/
/*                                                                      		*/
/* Locales : 	prec: pointeur précédent, motif_aux: copie de motif						*/
/* ------------------------------------------------------------------------ */
void RechercheMotif(Noeud_t *a, char *motif){
	Noeud_t ** prec;
	char motif_aux[20];

	strcpy(motif_aux, motif);
	printf("\nMot(s) commençant par le motif '%s' :\n", motif);
	prec = Recherche(&a,&motif);
	if(prec && *prec)
		AffichageArbre(*prec, motif_aux);
	printf("\n");
}

/* ------------------------------------------------------------------------ */
/* LibererArbre     		Libére la mémoire occupée par l'arbre	   						*/
/*                                                                      		*/
/* En entrée: a pointeur sur le premier noeud de l'arbre 			 							*/
/*			  nbEexp nombre de caractéres dans le fichier 											*/
/*                                                                      		*/
/* Locales : 	cour: pointeur cour sur l'arbre, n: Noeud d'itération					*/
/* 			p: pile pour la dérécursification, code: code de retour 						*/
/* ------------------------------------------------------------------------ */
void LibererArbre(Noeud_t *a) {
	donnee_t cour;
	Noeud_t *n;
	pile_t *p=NULL;
	int code;

	p=initPile(30);
	cour=a;
	while(cour!=NULL)
	{
		if(cour->vertical != NULL)
		{
			n = cour->vertical;
			cour->vertical = NULL;
			empiler(p,cour);
			cour = n;
		}
		else
		{
			if(cour->horizontal != NULL)
			{
				n = cour->horizontal;
				cour->horizontal = NULL;
				free(cour);
				cour = n;
			}
			else
			{
				free(cour);
				if(estPileVide(p) == 0)
				{
					cour = depiler(p,&code);
				}
				else
				{
					cour=NULL;
				}
			}
		}
	}
	libererPile(p);
}
