/* -------------------------------------------------------------------- */
/* 						Fichier d'entête arbre    						*/
/*                                                             			*/
/* Contient les prototypes des fonctions de traitement des arbres.		*/
/* Egalement nous avons la structure qui définit un noeud d'arbre.		*/
/*																		*/
/* -------------------------------------------------------------------- */
#ifndef ARBRE
#define ARBRE

#include "pile.h"
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* ------------------------------------ */
/* 			Structure d'un noeud   					*/
/* ------------------------------------ */
typedef struct cellule
{
	char c;
	struct cellule *vertical;
	struct cellule *horizontal;
}Noeud_t;

/* -------------------------------- */
/* 			Prototypes   			*/
/* -------------------------------- */
Noeud_t * InitNoeud();
Noeud_t * Insertion(Noeud_t **prec, char c);
void InsertionMot(Noeud_t ** a , char* m);
void AffichageArbre(Noeud_t *a, char *motif);
Noeud_t** Recherche(Noeud_t **a , char ** m);
void RechercheMotif(Noeud_t *a, char *motif);
void LibererArbre(Noeud_t *a);


#endif
