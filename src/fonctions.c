#include "fonctions.h"

/* ------------------------------------------------------------------------------------ */
/* Chargement          			Charge le fichier 		                    			*/
/*                                                                      				*/
/* En entrée: nomFichier chaîne de caractéres correspondant au chemin du fichier		*/
/*                                                                      				*/
/* En sortie: ligne chaîne de caractéres à renvoyer 									*/
/*                                                                      				*/
/* Locales : f: descripteur de fichier, i: index, nbChar: nombre de caractères du mot 	*/
/* 			mots: tableau contenant les mots											*/
/* ------------------------------------------------------------------------------------ */

char ** Chargement(int *error, int *nbMots, char *nomFichier)
{
	FILE *f;
	int i, nbChar;
	char **mots=NULL;

	f=fopen(nomFichier,"r");
	if(!f)
	{
		fprintf(stderr, "Probléme d'ouverture de fichier !\n");
		*error = EXIT_FAILURE;
	}
	else
	{
		fscanf(f, "%d\n", nbMots);
		mots = (char**)malloc(sizeof(char*)*(*nbMots));
		if (!mots)
		{
			fprintf(stderr, "Problème d'allocation !\n");
			*error = EXIT_FAILURE;
		}
		else {
			for(i=0; i<*nbMots; ++i)
			{
				mots[i] = (char *)malloc(sizeof(char)*30);
				if (!mots[i])
				{
					fprintf(stderr, "Problème d'allocation !\n");
					*error = EXIT_FAILURE;
				}
				else{
					fgets(mots[i], 30, f);
					nbChar = strlen(mots[i]);
					if(mots[i][nbChar-1] == '\n')
						mots[i][nbChar-1] = '\0';
				}
			}
		}
		fclose(f);
	}

	return mots;
}

/* ------------------------------------------------------------------------------------ */
/* LibererMots          			Libère la mémoire du tableau de mots 		        */
/*                                                                      				*/
/* En entrée: mots: tableau de mots, nbMots: nombre de mots du tableau					*/
/*                                                                      				*/
/* Locales : i: index																		*/
/* ------------------------------------------------------------------------------------ */
void LibererMots(char ** mots, int nbMots)
{
	int i;

	for (i = 0; i < nbMots; ++i)
	{
		free(mots[i]);
	}
	free(mots);
}
