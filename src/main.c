#include "fonctions.h"

/* -------------------------------------------------------------------- */
/* main        Programme principal                                      */
/*                                                                      */
/* En entrée: 														                              */
/*                                                                      */
/* En sortie: code de retour de la fonction pour le processus entier    */
/*                                                                      */
/* Locales : 	mots: tableau de mots, motif: chaîne du motif à rechercher	*/
/* 			error: code de retour des fonctions, nbMots: nombre de mots	*/
/* 			i: index, a: arbre											*/
/* -------------------------------------------------------------------- */
int main()
{
	char **mots;
	int error, nbMots = 0;
	Noeud_t *a=NULL;

	/* ------------------- TESTS ------------------- */

	mots = Chargement(&error,&nbMots,"src/dico.txt");


	puts("------------------- TESTS POUR LA FONCTION D'INSERTION -------------------");
	a=NULL;

	puts("Insertion dans un arbre vide");
	InsertionMot(&a,mots[1]);
	AffichageArbre(a, NULL);

	puts("Insertion en tête");
	InsertionMot(&a,mots[2]);
	AffichageArbre(a, NULL);

	puts("Insertion d’un mot qui existe déjà");
	InsertionMot(&a,mots[2]);
	AffichageArbre(a, NULL);

	puts("Insertion d’une partie d’un mot qui existe déjà");
	InsertionMot(&a,mots[0]);
	AffichageArbre(a, NULL);

	puts("Insertion d’un mot ayant une de sa partie existante");
	InsertionMot(&a,mots[3]);
	AffichageArbre(a, NULL);
	puts("------------------- FIN TESTS POUR LA FONCTION D'INSERTION -------------------");

	printf("\n");

	puts("------------------- TESTS POUR LA FONCTION DE RECHERCHE -------------------");
	LibererArbre(a);
	a=NULL;

	puts("Arbre vide");
	printf("L'adresse de retour est-elle celle de a ? %d\n", (void*)Recherche(&a, &(mots[0])) == &a);

	puts("------------------- FIN TESTS POUR LA FONCTION DE RECHERCHE -------------------");

	printf("\n");

	puts("------------------- TESTS POUR LA FONCTION D'AFFICHAGE -------------------");
	LibererArbre(a);
	a=NULL;
	puts("Arbre vide");
	AffichageArbre(a, NULL);
	puts("Affichage avec que des liens horizontaux");
	InsertionMot(&a,mots[4]);
	InsertionMot(&a,mots[5]);
	InsertionMot(&a,mots[6]);
	AffichageArbre(a, NULL);

	LibererArbre(a);
	a=NULL;

	puts("Affichage avec que des liens verticaux");
	InsertionMot(&a,mots[0]);
	InsertionMot(&a,mots[1]);
	InsertionMot(&a,mots[3]);
	AffichageArbre(a, NULL);

	LibererArbre(a);
	a=NULL;

	puts("Cas général");
	InsertionMot(&a,mots[0]);
	InsertionMot(&a,mots[1]);
	InsertionMot(&a,mots[3]);
	InsertionMot(&a,mots[4]);
	InsertionMot(&a,mots[5]);
	InsertionMot(&a,mots[6]);
	AffichageArbre(a, NULL);

	puts("------------------- FIN TESTS POUR LA FONCTION D'AFFICHAGE -------------------");
	printf("\n");


	puts("------------------- TESTS POUR LA FONCTION DE RECHERCHE D'UN MOTIF -------------------");
	printf("\n" );
	puts("Recherche d'un motif inexistant");
	RechercheMotif(a, "voiture");

	puts("Recherche d'un motif existant");
	RechercheMotif(a, "Tes");

	puts("------------------- FIN TESTS POUR LA FONCTION DE RECHERCHE D'UN MOTIF -------------------");


	/* ------------------- FIN TESTS ------------------- */

	LibererMots(mots,nbMots);
	LibererArbre(a);

	return 0;
}
